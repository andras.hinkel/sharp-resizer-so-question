import * as fs from 'fs';
import { piexif } from "piexifjs";
import resize from './resizer.mjs';

const targetSizes = [
  {
    width: 120,
    height: 120,
    filename: "something.jpg",
  },
];

console.log('targetSizes');
console.log(targetSizes);

const image = fs.readFileSync("./source/image-with-metadata.jpg");
const originalImageMetadata = piexif.load(image.toString("binary"));

const results = await resize(targetSizes, image);
const resultBuffer = await results[0].value.image.toBuffer();

const resizedImageMetadata = piexif.load(resultBuffer.toString("binary"));

console.log("originalImageMetadata");
console.log(originalImageMetadata);
console.log("resizedImageMetadata");
console.log(resizedImageMetadata);