import sharp from "sharp";

const resize = async (sizes, sourceImageBuffer) => {
  const resizeResults = await Promise.allSettled(
    sizes.map(({ width, height, filename }) => ({
      filename,
      image: sharp(sourceImageBuffer, { limitInputPixels: false })
        .withMetadata()
        .resize({
          width,
          height,
          fit: "inside",
        }),
    }))
  );

  const rejectedResizes = resizeResults.filter(
    ({ status }) => status === "rejected"
  );

  rejectedResizes.forEach((i) => console.error(`Failed to resize image: ${i}`));

  return resizeResults.filter(
    ({ status }) => status === "fulfilled"
  );
};

export default resize;